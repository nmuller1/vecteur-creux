// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe abstraite Tree

#ifndef Tree_hpp
#define Tree_hpp

template<typename U>
class Tree{
private:
    //attribut
    U _info;
    
protected:
    //constructeurs
    Tree() noexcept=default;
    Tree(U value): _info(value) {}
    Tree(Tree const& other) noexcept=default;
    //opérateur d'affectation
    virtual Tree& operator=(Tree const& other) noexcept=default;

public:

    //getters et setters
    virtual U getInfo() const {return this->_info;};
    virtual void setInfo(U value){_info=value;};
    
    //destructeur
    virtual ~Tree() noexcept = default;
};

#endif /* Tree_hpp */
