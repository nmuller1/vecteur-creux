// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe ABR

using namespace std;

#include "BinaryTree.hpp"
#include <vector>
#ifndef ABR_hpp
#define ABR_hpp

template<typename U>
class ABR: public BinaryTree<U>{
public:
    //constructeurs et destructeurs
    ABR():BinaryTree<U>(){}
    ABR(U value, ABR* parent=nullptr, ABR* left=nullptr, ABR* right=nullptr): BinaryTree<U>::BinaryTree(value,left,right,parent){}
    ABR(ABR const& other):BinaryTree<U>(other){}
    virtual ~ABR() noexcept = default;
    
    //méthodes
    void insert(U value); //insère un sous-arbre de valeur "value" au bon endroit dans l'ABR
    ABR<U>* research (U value) const; //renvoie un pointeur vers le sous-ABR de valeur correspondant "value" ou nullptr s'il n'existe pas
private:
    ABR* getNext() const;
};

template <typename U>
void ABR<U>::insert(U value){
    ABR<U> *current = this;
    ABR<U> *p = current;
    while (current != nullptr){
        p=current;
        if(value < p->getInfo()){
            current=static_cast< ABR<U>* >(current->getLeft());
        }
        else
        {
            current=static_cast< ABR<U>* >(current->getRight());
        }
        if(value < p->getInfo()){
            p->setLeft(new ABR<U>(value,p));
        }
        else{
            p->setRight(new ABR<U>(value,p));
        }
    };
}

template <typename U>
ABR<U>* ABR<U>::research (U value) const{
    ABR<U> *current = this;
    ABR<U> *p = current;
    while ((current != nullptr) && !(value == current->getInfo())){
        if(value < p->getInfo()){
            current=static_cast< ABR<U>* >(current->getLeft());
        }
        else
        {
            current=static_cast< ABR<U>* >(current->getRight());
        }
    }
    return current;
}

template <typename U>
ABR<U>* ABR<U>::getNext() const{
    ABR<U>* current;
    if(this->_right!=nullptr){
        current=static_cast< ABR<U>* >(current->getRight());
        while (current->getLeft()!=nullptr) {
            current=static_cast< ABR<U>* >(current->getLeft());
        }
    }else{
        ABR<U>* prev = this;
        current=static_cast< ABR<U>* >(this->getParent());
        while(current !=nullptr && static_cast<ABR<U>*>(current->getRigh()==prev)){
            prev=current;
            current=static_cast< ABR<U>* >(this->getParent());
        }
    }
    return current;
}
#endif /* ABR_hpp */
