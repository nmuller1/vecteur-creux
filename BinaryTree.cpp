// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe Binary Tree

#include "Tree.hpp"
#ifndef BinaryTree_hpp
#define BinaryTree_hpp

template<typename U>
class BinaryTree:public Tree<U> {
private:
    //attributs
    BinaryTree *_parent;
    BinaryTree *_left;
    BinaryTree *_right;
protected:
    //constructeurs
    BinaryTree():Tree<U>(),_left(nullptr),_right(nullptr),_parent(nullptr){}
    BinaryTree(U value, BinaryTree* parent=nullptr, BinaryTree* left=nullptr, BinaryTree* right=nullptr):Tree<U>(value),_parent(parent),_left(left),_right(right) {}
    BinaryTree(BinaryTree const& other):Tree<U>(other.getInfo())
    {
        _parent=new BinaryTree(*(other._parent));
        _right=new BinaryTree(*(other._right));
        _left=new BinaryTree(*(other._left));
    }
    //opérateur d'affectation
    BinaryTree& operator=(BinaryTree const& other)
    {
        if(this!=&other)
        {
            this->getInfo()=other.getInfo();
            delete _parent;
            delete _left;
            delete _right;
            _parent=new BinaryTree((*other._parent));
            _right=new BinaryTree((*(other._right)));
            _left=new BinaryTree((*(other._left)));
        }
        return *this;
    }

    //getters et setters
    BinaryTree* const getParent(){
        return _parent;
    }
    BinaryTree* const getLeft(){
        return _left;
    }
    BinaryTree* const getRight(){
        return _right;
    }
    void setParent(BinaryTree* parent){
        _parent=parent;
    }
    void setLeft(BinaryTree* left){
        _left=left;
    }
    void setRight(BinaryTree* right){
        _right=right;
    }

public :
    
    //destructeur
    virtual ~BinaryTree(){
        delete _parent;
        delete _right;
        delete _left;
    }
};
#endif /* BinaryTree_hpp */
