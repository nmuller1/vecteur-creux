// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe Tableau

#ifndef Tableau_hpp
#define Tableau_hpp

template <typename T>
class Tableau {
private:
    //attribut
    int size;
protected:
    //constructeurs et destructeur
    Tableau() noexcept=default;
    Tableau(int n): size(n){}
    Tableau(Tableau const& other) noexcept=default;
    
    //getter et setter
    virtual T getElem(int indice) const =0;
    virtual void setElem(int indice, T value)=0;
    int getSize() const {return size;};
public:
    virtual ~Tableau() = default;
};




#endif /* Tableau_hpp */
