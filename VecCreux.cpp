// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe VecteurCreux

#include "Tableau.hpp"
#include "ABR.hpp"
#include "Duet.hpp"
#ifndef VecCreux_h
#define VecCreux_h

template <typename T>
class VecCreux : public Tableau<T>, public ABR<Duet<T>>{
public:
    VecCreux(int size):Tableau<T>(size) {}
    VecCreux(VecCreux const& other):Tableau<T>(other.getSize()),ABR<Duet<T>>(other){}
    
    T getElem(int indice) const override {
        T value;
        if (indice < this->getSize()){
            Duet<T> Elem(indice);
            ABR<Duet<T>> *res=this->research(Elem);
            
            if (res == nullptr){
                value=0;
            }else{
                value = (res->getInfo()).getValue();
            }
        }
        else{
            throw "Cet indice fait référence à une position hors du tableau";
            }
        return value;
        }

    void setElem(int indice, T value) override{
        if (indice < this->getSize()){
            Duet<T> Elem(value,indice);
            ABR<Duet<T>> *res = this->research(Elem);
            if (res == nullptr){
                ABR<Duet<T>>::insert(Elem);}
            else{
                (res->getInfo()).setValue(value);}
        }else{
            std::cerr << "Cet indice fait référence à une position hors du tableau" << std::endl;
        }
    }
};
#endif /* VecCreux_h */
