// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe Duet

#ifndef Duet_hpp
#define Duet_hpp

template <typename T>
class Duet {
private:
    int _indice;
    T _value;

public:
    Duet() noexcept=default;
    Duet(int indice,T value=NULL):_indice(indice),_value(value){}
    
    T getValue() const {return _value ;}
    void setValue(T value){_value=value;};
    
    bool operator==(Duet const& other){
        return _indice==other._indice;
    }
    bool operator<(Duet const& other){
        return _indice<other._indice;
    }
};

#endif /* Duet_hpp */


