// Projet C++
// INFO-F-202
// Noëmie Muller
// 000458865
// 21-12-2018
// Classe ABRIterator

#ifndef ABRIterator_hpp
#define ABRIterator_hpp

template <typename U>
class ABRIterator {
    friend class ABR<U>;
private:
    ABR<U> *current;
    ABRIterator<U>& operator=(const ABROperator<T>& other);
    ABRIterator<U>& operator++();
    ABRIterator<U>& begin();
    ABRIterator<U>& end();
    U& operator*();
    U* operator->();
    bool operator!=(ABRIterator const& other);
public :
    ABRIterator(const ABR<U>* root);
    ABRIterator(const ABRIterator& other);
    ~ABRIterator() noexcept = default;
};

ABRIterator<U>::ABRIterator(const ABR<U>* root){
    current=root;
    for (ABRIterator& iter = begin(); iter != end(); iter++)
        cout << (*iter) << endl;
}
